package com.example.testjenkinsproject;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class SampleControllerTest {

    @Autowired
    SampleController sampleController;

    @Test
    @DisplayName("Hello World! 를 잘 반환합니다.")
    void helloWorld() {
        assertEquals(sampleController.helloWorld(), "Hello World!");
    }
}