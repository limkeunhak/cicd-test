package com.example.testjenkinsproject;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SampleController {
    @RequestMapping("/")
    public String helloWorld() {
        return "Hello World!";
    }
}
